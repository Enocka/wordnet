import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Queue;
import java.util.Iterator;

public class SAP {
    private Digraph G;
    private boolean[] markedInPathFromV;
    private boolean[] markedInPathFromW;
    private int[] edgeTo;
    private boolean[] dfsMarked;
    private int[] distanceToFromV;
    private int[] vSourceWithShortestDistance;
    private int[] distanceToFromW;
    private int currentShortestDistance;
    private int currentAncestor;
    private boolean bfsFound;
    private boolean multisourceBfsFound;
    private SET<Integer> possibleAncestors;
    private Stack<Integer> pathFromVToW, oldPathFromVToW;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G){
        this.G = G;
        markedInPathFromV = new boolean[G.V()];
        markedInPathFromW = new boolean[G.V()];
        edgeTo = new int[G.V()];
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w){
        if(v == w) return 0;
        bfs(v, w);
        if(currentShortestDistance != Integer.MAX_VALUE) return currentShortestDistance;
        return -1;
    }

    private void bfs(int v, int w){
//        System.out.println("bfs inacalliwa");
        markedInPathFromV = new boolean[G.V()];
        markedInPathFromW = new boolean[G.V()];
        edgeTo = new int[G.V()];
        distanceToFromV = new int[G.V()];
        distanceToFromW = new int[G.V()];
        currentShortestDistance = Integer.MAX_VALUE;
        Queue<Integer> qFromV = new Queue<Integer>();
        Queue<Integer> qFromW = new Queue<Integer>();
        possibleAncestors = new SET<Integer>();
//        System.out.println("tunaanza first trip");
        int parentNode = v;
        markedInPathFromV[v] = true;
        possibleAncestors.add(parentNode);
        for(int i = 0; i < G.V(); i++){
            distanceToFromV[i] = 0;
            distanceToFromW[i] = 0;
        }
        qFromV.enqueue(v);
//        System.out.println("tumeanza na 1 ... "+v);
        while (!qFromV.isEmpty()){
            System.out.println();
            parentNode = qFromV.dequeue();
//            System.out.println("distanceToFromV ya "+parentNode+" ni "+distanceToFromV[parentNode]);

            for (int childNode : G.adj(parentNode)){
//                System.out.println("childnode ya kwanza ya "+parentNode+" ni 0 ..."+childNode);
                if(!markedInPathFromV[childNode]){
//                    System.out.println("tuimark kwanza, so markedInPathFromV"+childNode);
                    markedInPathFromV[childNode] = true;
                    distanceToFromV[childNode] = distanceToFromV[parentNode] + 1;
//                    System.out.println("na distance from v ya "+childNode+" ni 1 ... "+distanceToFromV[childNode]);
                    edgeTo[childNode] = parentNode;
                    possibleAncestors.add(childNode);
                    qFromV.enqueue(childNode);
//                    System.out.println("tumeadd 0 ..."+childNode+" kwa possible ancestors");
//                    System.out.println("alafu tukaenqueue 0 "+ childNode);
                }

            }


            if (parentNode == w) {
                currentShortestDistance = distanceToFromV[parentNode];
                currentAncestor = parentNode;
                bfsFound = true;
                return;
            };

        }

//        System.out.println("bfs inacalliwa, na shortest distance ni "+ currentShortestDistance);
//        System.out.println();
//        System.out.println();
//        System.out.println("tunaanza second trip");
        qFromW = new Queue<Integer>();
        markedInPathFromW = new boolean[G.V()];
        parentNode = w;
        markedInPathFromW[w] = true;
//        System.out.println("tumeanza na  "+w);
        bfsFound = false;
        markedInPathFromW[w] = true;
        edgeTo[w] = w;
        qFromW.enqueue(parentNode);
        while (!qFromW.isEmpty() && !bfsFound ){
            parentNode = qFromW.dequeue();
//            System.out.println();
//            System.out.println("distanceToFromW ya "+parentNode+" ni "+distanceToFromW[parentNode]);
            if(possibleAncestors.contains(parentNode)){
//                System.out.println("alafu tukaenqueue 0 "+ childNode);
                if((distanceToFromW[parentNode] + distanceToFromV[parentNode]) < currentShortestDistance){
//                    System.out.println("na distanceToFromV[0] "+distanceToFromV[parentNode] + " plus distanceToFromW[0] " + distanceToFromW[0]+" ni less than "+ Integer.MAX_VALUE + currentShortestDistance);
                    currentShortestDistance = distanceToFromW[parentNode] + distanceToFromV[parentNode];
                    currentAncestor = parentNode;
                    bfsFound = true;
                    return;
                }
            }
            for (int childNode : G.adj(parentNode)){
                edgeTo[childNode] = parentNode;
                distanceToFromW[childNode] = distanceToFromW[parentNode] + 1;
                qFromW.enqueue(childNode);
//                System.out.println("childnode ya kwanza ya "+parentNode+" ni ..."+childNode);
                if(!markedInPathFromW[childNode]){
                    markedInPathFromW[childNode] = true;
                    if(possibleAncestors.contains(childNode)){
//                        System.out.println("na "+ childNode+" ni possible ancestor");
                        if((distanceToFromW[childNode] + distanceToFromV[childNode]) < currentShortestDistance){
//                            System.out.println("na distanceToFromV[0] "+distanceToFromV[childNode] + " plus distanceToFromW[0] " + distanceToFromW[0]+" ni less than "+ Integer.MAX_VALUE + currentShortestDistance);
                            currentShortestDistance = distanceToFromW[childNode] + distanceToFromV[childNode];
                            currentAncestor = childNode;
                            bfsFound = true;
                            return;
                        }
                    }
                    if (parentNode == v) {
                        currentShortestDistance = distanceToFromW[parentNode];
                        currentAncestor = v;
                        bfsFound = true;
                        return;
                    };


                }
            }
        }
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w){
        if(v == w) return v;
        bfs(v, w);
        if (length(v, w) == -1) return -1;
        if(bfsFound) return currentAncestor;
        return -1;
    }

    private void multisourceBfs(Iterable<Integer> v, Iterable<Integer> w){
        boolean[] marked;
        marked = new boolean[G.V()];
        edgeTo = new int[G.V()];
        distanceToFromV = new int[G.V()];
        currentShortestDistance = Integer.MAX_VALUE;
        Queue<Integer> q = new Queue<Integer>();
        possibleAncestors = new SET<Integer>();
        for(int i = 0; i < G.V(); i++){
            distanceToFromV[i] = 0;
            distanceToFromW[i] = 0;
        }
        for(int i : v){
            q.enqueue(i);
            marked[i] = true;
        }
        int x;
        while (!q.isEmpty()){
            x = q.dequeue();
            for (int i : G.adj(x)){
                if(!marked[i]){
                    q.enqueue(i);
                    marked[i] = true;
                    if (distanceToFromV[x] + 1 < distanceToFromV[i])
                        distanceToFromV[i] = distanceToFromV[x] + 1;
                    edgeTo[i] = x;
                    possibleAncestors.add(i);
                }
            }
        }

        q = new Queue<Integer>();
        marked = new boolean[G.V()];
        distanceToFromW = new int[G.V()];
        for(int i : w){
            q.enqueue(i);
            marked[i] = true;
        }
        while (!q.isEmpty()){
            x = q.dequeue();
            for (int i : G.adj(x)){
                if(!marked[i]){
                    distanceToFromW[i] = distanceToFromW[x] + 1;
                    if(possibleAncestors.contains(i)){
                        if((distanceToFromW[i] + distanceToFromV[i]) < currentShortestDistance){
                            currentShortestDistance = distanceToFromW[i] + distanceToFromV[i];
                            currentAncestor = i;
                            multisourceBfsFound = true;
                        }
                    }
                    q.enqueue(i);
                    marked[i] = true;
                    edgeTo[i] = x;
                }
            }
        }
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w){
        multisourceBfs(v, w);
        return -1;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w){
        multisourceBfs(v, w);
        if(multisourceBfsFound) return currentAncestor;
        return -1;
    }

    // do unit testing of this class
    public static void main(String[] args) {
//        In in = new In(args[0]);
//        Digraph G = new Digraph(13);
//        G.addEdge(7, 3);
//        G.addEdge(7,  3);
//        G.addEdge(8,  3);
//        G.addEdge(3,  1);
//        G.addEdge(4,  1);
//        G.addEdge(5,  1);
//        G.addEdge(9,  5);
//        G.addEdge(10, 5);
//        G.addEdge(11, 10);
//        G.addEdge(12, 10);
//        G.addEdge(1,  0);
//        G.addEdge(2,  0);
//        SAP sap = new SAP(G);
//        int length = sap.length(11, 6);
//        System.out.println("length "+length);
//        Digraph G = new Digraph(6);
//        G.addEdge(1, 0);
//        G.addEdge(1,  2);
//        G.addEdge(2,  3);
//        G.addEdge(3,  4);
//        G.addEdge(4,  5);
//        G.addEdge(5,  0);
//        SAP sap = new SAP(G);
//        int length = sap.length(4, 2);
//        System.out.println("length "+length);
        Digraph G = new Digraph(9);
        G.addEdge(1, 7);
        G.addEdge(7, 8);
        G.addEdge(8, 2);
        G.addEdge(5,  0);
        G.addEdge(2,  6);
        G.addEdge(6,  0);
        G.addEdge(0,  3);
        G.addEdge(4,  3);
//        G.addEdge(3,  2);
        SAP sap = new SAP(G);
//        int length = sap.length(1, 3);
        int length = sap.length(2, 1);
        System.out.println("length "+length);

        System.out.println();
        System.out.println();

//        length = sap.length(1, 2);
//        System.out.println("length "+length);

    }
}


//while (!StdIn.isEmpty()) {
//        int v = StdIn.readInt();
//        int w = StdIn.readInt();
//        int length   = sap.length(v, w);
//        int ancestor = sap.ancestor(v, w);
//        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
//}
//    digraph G {
//        7 -> 3
//        8 -> 3
//        3  ->1
//        4 -> 1
//        5 -> 1
//        9  ->5
//        10 -> 5
//        11 ->10
//        12 ->10
//        1  ->0
//        2 -> 0
//        }
//
//
//        digraph G {
//        1 -> 0
//        1 ->2
//        2 ->3
//        3 ->4
//        4 ->5
//        5 ->0
//        }
//
//        digraph G {
//        1 -> 2
//        2  ->3
//        3  ->4
//        4  ->5
//        5  ->6
//        6  ->1
//        7  ->8
//        8  ->9
//        9 ->10
//        10 ->11
//        11 ->12
//        12  ->8
//        13 ->14
//        14  ->0
//        0 ->11
//        }

