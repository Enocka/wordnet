import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ST;
import edu.princeton.cs.algs4.SET;
import java.util.Iterator;

public class WordNet {
    //a store for the nouns, quickly check if it exists
    //also tracks the set of synsets in which it appears
    private final ST<String, SET<Integer>> nounSynsetsSet;
    //a store to dereference
    private final ST<Integer, String> synsetST;
    private final Digraph nounDigraph;
    private final SAP sap;
//    private final Bag<String>[] synonymsBag;
    
    // linearithmic
    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms){
        In synsetsIn = new In(synsets);
        In hypernymsIn = new In(hypernyms);
        int synsetVertexCount = 0;
        String line;
        int lineNumber;
        String[] lineNouns;
        nounSynsetsSet = new ST<String, SET<Integer>>();
        synsetST = new ST<Integer, String>();
        while(synsetsIn.hasNextLine()){
            line = synsetsIn.readLine();
            lineNumber = Integer.parseInt(line.split(",")[0]);
            synsetST.put(lineNumber, line.split(",")[1]);
            lineNouns = line.split(",")[1].split(" ");
            for(int i = 0; i < lineNouns.length; i++){
                SET<Integer> synsetsSet;
                if(nounSynsetsSet.get(lineNouns[i]) == null){
                    synsetsSet = new SET<Integer>();
                    synsetsSet.add(lineNumber);
                }else {
                    synsetsSet = nounSynsetsSet.get(lineNouns[i]);
                    synsetsSet.add(lineNumber);
                }
                nounSynsetsSet.put(lineNouns[i], synsetsSet);
            }
            synsetVertexCount++;
        }
        nounDigraph = new Digraph(synsetVertexCount);
        String[] lineHypernyms;
        while(hypernymsIn.hasNextLine()){
            line = hypernymsIn.readLine();
            lineHypernyms = line.split(",");
            for (int i = 1; i < lineHypernyms.length; i++){
                nounDigraph.addEdge(Integer.parseInt(lineHypernyms[0]), Integer.parseInt(lineHypernyms[i]));
            }
        }
        sap = new SAP(nounDigraph);
    }

    // returns all WordNet nouns
    public Iterable<String> nouns(){
        return nounSynsetsSet.keys();
    }

    // logarithmic
    // is the word a WordNet noun?
    public boolean isNoun(String word){
        return nounSynsetsSet.contains(word);
    }

    // linear 2 digraph
    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB){
        return sap.length(nounSynsetsSet.get(nounA), nounSynsetsSet.get(nounB));
    }

    // linear 2 digraph
    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB){
        int ancestor = sap.ancestor(nounSynsetsSet.get(nounA), nounSynsetsSet.get(nounB));
        return synsetST.get(ancestor);
    }

    // do unit testing of this class
    public static void main(String[] args){

    }
}